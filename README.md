TwitterConnect
==============

Integrating twitter in your android application will make user easily login into your app using their twitter account which avoids filling a long registration forms.

 In this demo i explained how to integrate twitter in your android application using twitter oAuth procedure. This is very basic tutorial with simple login and updating twitter status message.
 
 Registering twitter app – getting Consumer Key & Consumer Secret
In order to implement twitter oAuth in your application you need twitter consumer key and consumer secret which are used to make twitter API calls. So register a new twitter application and get the keys. Check the following video about creating a twitter app

1. Go to https://dev.twitter.com/apps/new and register new application. Fill application name, description and website.

2. Give some dummy url in the callback url field to make the app as browser app. (If you leave it as blank it will act as Desktop app which won’t work in mobile device)

3. Under the settings tab upload icon and change the access type to Read and Write.

4. Copy Consumer Key & Consumer Secret key

Downloading twitter4j library
In this demo to integrate twitter i used one of the most popular libraries twitter4j.

You can read their official documentation to know more about it.here http://twitter4j.org/en/code-examples.html

1. Download & extract twitter4j library from twitter4j-android-2.2.6.zip (slimmed version for Android platform). Here is the direct link

Download & extract twitter4j library from twitter4j-android-2.2.6.zip (slimmed version for Android platform).

Here is the direct link http://twitter4j.org/en/index.html#download
or
https://code.google.com/p/google-api-java-client/wiki/Setup#Android
or


2. Copy the required twitter4j .jar files into your project’s libs folder. In this demo i copied twitter4j-core-android-2.2.6 as it is the only required file in this demo.

